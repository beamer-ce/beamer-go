package beamer

/*Match type */
type Match struct {
	id    int
	team1 string
	team2 string
}

/*Details foo */
func (m Match) Details() (int, string, string) {
	return m.id, m.team1, m.team2
}
